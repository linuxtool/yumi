EFI YUMI (Your Universal Multiboot Integrator) �2011-2017 Lance http://www.pendrivelinux.com (covered under GNU GPL License) - see YUMI-Copying

Background of YUMI:

YUMI is an easy to use Multiboot script created using NSIS. YUMI's purpose is to help automate the creation of a bootable USB Flash Drive that can be used to boot multiple Linux based distributions (one at a time).
The end result should be a bootable USB Flash drive that will get you up and running with your chosen Live Distributions, all without having to do the research and perform the steps by hand. 

How YUMI Works:

YUMI utilizes a Syslinux MBR to make the chosen drive bootable. Syslinux then hands control over to GRUB2 for Legacy BIOS booting. For UEFI booting GRUB is nativaly used. Distribution ISO files are copied directly or extracted using 7zip to the multiboot folder on the USB device. Configuration files are used to house the menus used to boot each distribution. 

Credits, Resources, and Third Party Tools used:

* Remnants of Cedric Tissieres's Tazusb.exe for Slitaz (slitaz@objectif-securite.ch) may reside in the YUMI script, as it was derived from UUI, which was originally inspired by Tazusb.exe. 
* Created with NSIS Installer � Contributors http://nsis.sourceforge.net (needed to compile the YUMI.nsi script)  
* Syslinux � H. Peter Anvin http://syslinux.zytor.com (unmodified binary used)
* GRUB � Free Software Foundation, Inc. http://www.gnu.org/software/grub/grub.html
* 7-Zip is � Igor Pavlovis http://7-zip.org (unmodified binaries were used)
* Fat32format.exe � Tom Thornhill Ridgecorp Consultants http://www.ridgecrop.demon.co.uk (unmodified binary used)
* Tuffy Font � Thatcher Ulrich http://tulrich.com/fonts/ (unmodified binary used)

Additional instructions and information for YUMI can be found HERE: http://www.pendrivelinux.com/yumi-multiboot-usb-creator/

Changelog:
01/19/17 Version 0.0.0.4: Fix to replace empty spaces in Distro filename with dashes. Fix to display only FDD+HDD. Correct some wording.
08/19/16 Version 0.0.0.3: Add support for Arch Bang, Skywave Linux, and Cyborg Linux.
07/28/16 Version 0.0.0.2: Fix entry for Slacko Puppy UEFI
06/28/16 Version 0.0.0.1: Initial Release.